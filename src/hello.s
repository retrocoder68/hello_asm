;;;   Copyright (C) 2021 skywalker. All rights reserved   ;;;
; Author: skywalker aka J.Karlsson <j.karlsson@retrocoder.se>

; Exec library base
ExecBase             equ 4

; Library Vector Offsets
Exec.OpenLibrary    equ -552
Exec.CloseLibrary   equ -414
Dos.Output          equ -60
Dos.Write           equ -48

;--- Code section ---
  code

  xdef _start
_start:
  ; Open Dos library
  move.l ExecBase, a5
  move.l a5, a6
  lea dosname, a1
  moveq #0, d0
  jsr Exec.OpenLibrary(a6)

  ; Did it work?
  move.l d0, a6
  beq error

  ; Get a filehandle to the current console (stdout / stderr)
  jsr Dos.Output(a6)

  ; Print cool message 8-)
  move.l d0,d1
  move.l #hello, d2
  moveq #14, d3
  jsr Dos.Write(a6)

  ; Close Dos library
  move.l a6, a1
  move.l a5, a6
  jsr Exec.CloseLibrary(a6)

  ; Exit
  moveq #0, d0
error:
  rts

;--- Data section ---
  data

  even
dosname:  dc.b "dos.library", 0

  even
hello:    dc.b "Hello, world!\n", 0

; License:
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License version 2 as
; published by the Free Software Foundation.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program. If not, see <http://www.gnu.org/licenses/>
; or write to the Free Software Foundation, Inc.,
; 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

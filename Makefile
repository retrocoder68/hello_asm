###   Copyright (C) 2021 skywalker. All rights reserved   ###
# Author: skywalker aka J.Karlsson <j.karlsson@retrocoder.se>
# Target system: Amiga 500 (AmigaOS 1.3 - version 34)

# Tools
AS=vasmm68k_mot
LD=vlink
RM=del /F /Q
CP=copy

# Directory structure
SRC=src
BLD=build
OBJ=$(BLD)\obj
BIN=$(BLD)\bin

# Installation directory
INSTDIR=D:\OneDrive\Documents\retro\Amiga\drives\wb1_3\user\bin

# Source files (.s files in the src directory)
SOURCES=$(wildcard $(SRC)/*.s)

# Binary files
PROG=hello_asm
OBJS=$(subst $(SRC),$(OBJ),$(addsuffix .o,$(basename $(SOURCES))))

# Flags
ASFLAGS=-quiet -m68000 -phxass -align -chklabels -wfail -x -Fhunk -kick1hunks
LDFLAGS=-b amigahunk -sc -sd -s

# Targets
.PHONY: all install clean
all: $(BIN)\$(PROG) install

# Test targets
install: $(BIN)\$(PROG)
	$(CP) $(BIN)\$(PROG) $(INSTDIR)

# Link targets
$(BIN)\$(PROG): $(OBJS)
	$(LD) $(LDFLAGS) -o $@ $^

# Object files
$(OBJ)%.o: $(SRC)%.s
	$(AS) $(ASFLAGS) -o $@ $<

clean:
	$(RM) $(OBJ)\* $(BIN)\*

# License:
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>
# or write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
